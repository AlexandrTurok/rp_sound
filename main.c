#include <stdio.h>
#include "wav_player.h"

extern int play_file(const char * file_name);

int main(int argc, const char * argv[])
{
    /* Check cli arguments */
    if(argc < 2)
    {
        fprintf(stderr, "Must pass file to play\n");
        return 1;
    }

    return play_wav_file(argv[1]);;
}
